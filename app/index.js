import 'styles/index.scss';
import 'styles/flickity.css';

import Chart from './chart.js';
import Flickity from 'flickity';

function init() {
	let graphs = document.querySelectorAll('.graph');
	graphs.forEach((el) => {
		new Chart(el);
	});

	let flkty = new Flickity('.carousel', {
		prevNextButtons: false,
		cellAlign: 'left',
		freeScroll: true,
		contain: true
	});
}

window.addEventListener( 'load', init, false);