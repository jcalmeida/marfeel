import * as d3 from "d3";

export default class Chart {
   constructor(el) {
     this.el = el;
     this.chartContainer;

     const jsonSrc = this.el.getAttribute("data-json");

     if (jsonSrc) {
         d3.json(jsonSrc).then((data) => {
            this.data = data;
            this.build();
            this.el.classList.add('loaded');
         });
     }

   }

   build () {
      this.el.innherHtml = '';
      this.buildCharts();
      this.insertText();
   }

   insertText () {
      const textContainer = document.createElement('div');
      textContainer.className = 'graph-legend';

      this.data.donut.reverse().forEach((data) => {
         const number = this.formatNumber(data.value, this.data.currency);
         const percentage = (data.value * 100) / this.data.amount;

         let text = document.createElement('div');
         text.className = 'graph-legend__item';
         text.innerHTML = '<h4>' + data.name + '</h4><p><strong>' + parseInt(percentage) + '%</strong> ' + number + '</p>';
         textContainer.appendChild(text);    
      })

      this.el.appendChild(textContainer);
   }

   buildCharts () {
      this.chartContainer = document.createElement('div');
      this.chartContainer.className = 'graph__container';
      this.el.appendChild(this.chartContainer);

      let title = document.createElement('div');
      title.className = 'graph__title';
      title.innerHTML = '<h3>' + this.data.title + '</h3><h4>' + this.formatNumber(this.data.amount, this.data.currency) + '</h4>';
      this.chartContainer.appendChild(title);

      this.buildDonut();
      this.buildLine();
   }

   buildLine () {
      const margin = {top: 5, right: 0, bottom: 0, left: 0};
      const width = 160;
      const height = 55;
      const data = this.data.line;

      let x = d3.scaleLinear().range([0, width]);
      let y = d3.scaleLinear().range([height, 0]);

      let area = d3.area()
         .x(function(d) { return x(d.date); })
         .y0(height)
         .y1(function(d) { return y(d.value); });

      let valueline = d3.line()
         .x(function(d) { return x(d.date); })
         .y(function(d) { return y(d.value); });

      let svg = d3.select(this.chartContainer).append("svg")
         .attr("width", width + margin.left + margin.right)
         .attr("height", height + margin.top + margin.bottom)
         .attr('class','graph-line');
         
      let g = svg.append("g")
         .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      let parseTime = d3.timeParse("%d-%b-%y");

      data.forEach(function(d) {
         d.date = parseTime(d.date);
         d.value = +d.value;
      });

      x.domain(d3.extent(data, function(d) { return d.date; }));
      y.domain([0, d3.max(data, function(d) { return d.value; })]);

      g.append("path")
         .data([data])
         .attr("class", "area")
         .attr("d", area);

      g.append("path")
         .data([data])
         .attr("class", "line")
         .attr("d", valueline);
   }

   buildDonut () {
      const width = 160;
      const height = 160;
      const radius = Math.min(width, height) / 2;
      const thickness = 8;
      const data = this.data.donut;

      let svg = d3.select(this.chartContainer)
         .append('svg')
         .attr('class', 'graph-donut')
         .attr('width', width)
         .attr('height', height);

      const point_length = 12;
      const point_thickness = 2;
      
      let points = svg.append('g');

      for (let i = 0; i < 4; i++) {
         let w = (i < 2) ? point_thickness : point_length;
         let h = (i < 2) ? point_length : point_thickness;
         
         let x = width / 2;
         if (i == 2) {
            x = 0;
         } else if (i == 3) {
            x = width - point_length;
         }

         let y = height / 2;;
         if (i == 0) {
            y = 0;
         } else if (i == 1) {
            y = height - point_length;
         }

         points.append('rect')
            .attr('width', w)
            .attr('height', h)
            .attr('x', x)
            .attr('y', y)
            .attr('fill', '#A4A4A4');   
      }

      let g = svg.append('g')
         .attr('transform', 'translate(' + (width/2) + ',' + (height/2) + ')');

      let arc = d3.arc()
         .innerRadius(radius - thickness)
         .outerRadius(radius);

      let pie = d3.pie()
         .value(function(d) { return d.value; })
         .sort(null);

      let path = g.selectAll('path')
         .data(pie(data))
         .enter()
         .append("g")
           .attr('class', 'graph-donut__data')
           .append('path')
           .attr('d', arc);
   }

   formatNumber(number, currency) {
      let value = number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      if (currency) {
         value += '€';
      }

      return value;
   }
}